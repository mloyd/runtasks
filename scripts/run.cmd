@echo off
rem = """-*-Python-*- script
python -x "%~f0" %*
exit /b %errorlevel%
"""
import sys, os
pwd = os.getcwd()
if pwd not in sys.path:
    sys.path.insert(0, pwd)
from runtasks.cmdline import run
run()
